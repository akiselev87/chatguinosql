Don't forget to add Access-Control-Allow-Origin: * to all pages from the Tomcat server

Add to server tomcat web.xml:
<filter>
  <filter-name>CorsFilter</filter-name>
  <filter-class>org.apache.catalina.filters.CorsFilter</filter-class>
</filter>
<filter-mapping>
  <filter-name>CorsFilter</filter-name>
  <url-pattern>/*</url-pattern>
</filter-mapping>

Based on http://tomcat.apache.org/tomcat-8.0-doc/config/filter.html#CORS_Filter

Cassandra logs folder:
C:\var\lib\cassandra\commitlog


Database:
CREATE KEYSPACE IF NOT EXISTS chat
WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 3 };

ALTER KEYSPACE chat WITH REPLICATION = 
{ 'class' : 'SimpleStrategy', 'replication_factor' : 1 };


TRUNCATE chat.dictionary;
TRUNCATE chat.user;
TRUNCATE chat.user_link;
TRUNCATE chat.message;
TRUNCATE chat.session_security;
TRUNCATE chat.news_feed;


drop table IF EXISTS chat.dictionary;
drop table IF EXISTS chat.user;
drop table IF EXISTS chat.user_link;
drop table IF EXISTS chat.message;
drop table IF EXISTS chat.session_security;
drop table IF EXISTS chat.news_feed;

CREATE TABLE chat.dictionary (
  value_id    text,
  d_value     text, 
  d_name    text,
  PRIMARY KEY (d_name, value_id)
) WITH comment = 'dictionary values'
AND CLUSTERING ORDER BY (value_id ASC);

CREATE TABLE chat.user (
  user_id               uuid,
  email                  text, 
  first_name          text,
  last_name          text,
  user_password   text,
  avatar text,
  created_date      timestamp, 
  updated_date     timestamp,
  PRIMARY KEY (user_id)
) WITH comment = 'List of all registered and active users';

CREATE TABLE chat.session_security(
  id_security_key     uuid,
  id_user                  uuid, 
  created_date          timestamp,
  PRIMARY KEY (id_security_key)
) WITH comment = 'Security codes for sessions';

CREATE TABLE chat.user_link (
  id                         uuid,
  id_user               uuid,
  id_related_user  uuid,
  first_name          text,
  last_name          text,
  avatar text,
  email                  text,
  link_weght     int,
  id_dictionary      int,
  created_date     timestamp, 
  updated_date    timestamp,
  PRIMARY KEY (id)
) WITH comment = 'List of friendship status by user';

CREATE TABLE chat.message(
  id_message                           uuid,
  id_user_sent         uuid,
  id_user_link uuid,
  message               text, 
  image blob,
  image_preview text,
  image_name text,
  image_length int,
  image_resolution text,
  is_image int,
  created_date        timestamp,
  PRIMARY KEY ((id_user_link), created_date))
	WITH CLUSTERING ORDER BY (created_date DESC) AND comment = 'List of all messages by link id';

CREATE TABLE chat.news_feed(
  id_news_feed                           uuid,
  id_user_sent         uuid,
  first_name text,
  last_name text,
  avatar text,
  security int,
  news_feed text, 
  image blob,
  image_preview text,
  image_name text,
  image_length int,
  image_resolution text,
  is_image int,
  created_date        timestamp,
  created_date_text text,
  PRIMARY KEY ((id_user_sent), created_date))
	WITH CLUSTERING ORDER BY (created_date DESC) AND comment = 'List of all news feed by link id';

CREATE TABLE IF NOT EXISTS chat.news_feed_rating(
  id_rating                          uuid,
  id_news_feed        uuid,
  id_user_rated uuid,
  rate int,
  created_date        timestamp,
  PRIMARY KEY ((id_rating), created_date))
	WITH CLUSTERING ORDER BY (created_date DESC) AND comment = 'List of all ratings for the article';


INSERT INTO chat.dictionary (d_name,d_value,value_id) VALUES ('Frienship Status','Friendship Request Sent','0');
INSERT INTO chat.dictionary (d_name,d_value,value_id) VALUES ('Frienship Status','Friendship Requested','10');
INSERT INTO chat.dictionary (d_name,d_value,value_id) VALUES ('Frienship Status','Subscription Requested','12');
INSERT INTO chat.dictionary (d_name,d_value,value_id) VALUES ('Frienship Status','Subscribed, Friendship Requested','11');
INSERT INTO chat.dictionary (d_name,d_value,value_id) VALUES ('Frienship Status','Friends','20');
INSERT INTO chat.dictionary (d_name,d_value,value_id) VALUES ('Frienship Status','Subscribed','21');
INSERT INTO chat.dictionary (d_name,d_value,value_id) VALUES ('Frienship Status','Ignored','30');
INSERT INTO chat.dictionary (d_name,d_value,value_id) VALUES ('Security News Feed','Only Me','100');
INSERT INTO chat.dictionary (d_name,d_value,value_id) VALUES ('Security News Feed','Friends','110');
INSERT INTO chat.dictionary (d_name,d_value,value_id) VALUES ('Security News Feed','Everybody','120');

