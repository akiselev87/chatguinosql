(function () {
    'use strict';

angular.module('scopeRegistration', ['toaster','ngFileUpload','ngAnimate','ngImgCrop']).
controller('RegController', ['$scope', '$http','toaster', 'Upload', '$timeout',
function($scope, $http, toaster, Upload, $timeout) {

	const securityKey = cookieGet("securityKey");
	const host = cookieGet("host");
	const method = "GET";
	const url = "http://" + host + "/ChatRestNoSQLMaven/rest/AccountService/getAccountData/"+securityKey;

var responsePromise = $http.get(url);
responsePromise.success
	(function(data, status, headers, config) {
	$scope.fname = data.first_name;
	$scope.lname = data.last_name;
	$scope.email = data.email;
	$scope.avatar = data.avatar;
	});
responsePromise.error(function(data, status, headers, config) {
	toaster.pop("error","Error","Something went wrong with our database... Please, stay with us");
});
	
$scope.goToLink = function(toaster) {
	var match = toaster.body.match(/http[s]?:\/\/[^\s]+/);
	if (match) $window.open(match[0]);
	return true;
};

//AVATAR
$scope.size='medium';
$scope.type='square';
$scope.imageDataURI='';
$scope.resImageDataURI='';
$scope.resImgFormat='image/png';
$scope.resImgQuality=1;
$scope.selMinSize=40;
$scope.resImgSize=40;
/*$scope.aspectRatio=1.2;
$scope.onChange=function($dataURI) {
  console.log('onChange fired');
};
$scope.onLoadBegin=function() {
  console.log('onLoadBegin fired');
};
$scope.onLoadDone=function() {
  console.log('onLoadDone fired');
};
$scope.onLoadError=function() {
  console.log('onLoadError fired');
};
*/
var handleFileSelect=function(evt) {
  var file=evt.currentTarget.files[0];
  var reader = new FileReader();
  reader.onload = function (evt) {
    $scope.$apply(function($scope){
      $scope.imageDataURI=evt.target.result;
    });
  };
  reader.readAsDataURL(file);
};
angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);
$scope.$watch('resImageDataURI',function(){
//console.log('Res image', $scope.resImageDataURI);
});

// END OF AVATAR

$scope.reset = function () {
	$scope.oldPassword = null;
    $scope.password = null;
    $scope.passwordConfirm = null;
    $scope.regForm.$setUntouched();
};

$scope.resetName = function () {
   $scope.fname = null;
   $scope.lname = null;
   var responsePromise = $http.get(url);	
   responsePromise.success(function(data, status, headers, config) {
	    $scope.fname = data.first_name;
   		$scope.lname = data.last_name;
   		$scope.email = data.email;
   		$scope.avatar = data.avatar;
   });
   responsePromise.error(function(data, status, headers, config) {
	   toaster.pop("error","Error","Request failed!");
   });
   $scope.regForm.$setUntouched();
};

$scope.updateName = function () {	
	const email = escape($scope.email,"UTF-8");
	const fname = $scope.fname;
	const lname = $scope.lname;
	const urlModifyName = "http://" + host + "/ChatRestNoSQLMaven/rest/AccountService/updateAccountName/"+securityKey+"/"+fname+"/"+lname;
	var responsePromise = $http.get(urlModifyName);	
	responsePromise.success(function(data, status, headers, config) {
		if (data = 1){
			toaster.pop("success", "Success", "Updated. System will apply changes with the next log in");
		}
		else
	    {
			toaster.pop("error","Error","Update failed!");
	    }
	});
	responsePromise.error(function(data, status, headers, config) {
	    toaster.pop("error","Error","Request failed!");
	});	
};

$scope.uploadAvatar = function (dataURI) {	
	const urlUploadAvatar = "http://" + host + "/ChatRestNoSQLMaven/rest/AccountService/updateAvatar";
            Upload.upload({
              url: urlUploadAvatar,
              data: {
           	   securityKey: securityKey,
           	   avatar: dataURI  
              }
          }).then(function (resp) {
            	$timeout(function () {
            		$scope.resImageDataURI='';
        			$scope.avatar = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAV0lEQVRYR+3SsQ0AMAgEMdh/aYZwQ3HpT0LO7zx/+/y+6UD9oQQTVAHt22CCKqB9G0xQBbRvgwmqgPZtMEEV0L4NJqgC2rfBBFVA+zaYoApo3wYTVAHtDxz6ACmSkz/0AAAAAElFTkSuQmCC';
            		toaster.pop("success","File uploaded","Avatar successfully changed");
               });
          }, function (response) {
               if (response.status > 0) {
                  toaster.pop("error","Error","Unable to upload file. Please try again...");
               }
          });
	}

$scope.updatePassword = function () {	
	const oldPassword= escape($scope.oldPassword,"UTF-8");
	const password = escape($scope.password,"UTF-8");
	const passwordConfirm = escape($scope.passwordConfirm,"UTF-8");
	const urlModifyPassword = "http://" + host + "/ChatRestNoSQLMaven/rest/AccountService/updateAccountPwd/"+securityKey+"/"+oldPassword+"/"+password+"/"+passwordConfirm;
	var responsePromise = $http.get(urlModifyPassword);	
	responsePromise.success (function(data, status, headers, config) {
		if (data = 1){
			toaster.pop("success", "Success", "Updated, please do not forget your new password");
			$scope.regForm.$setUntouched();
			$scope.oldPassword = null;
		    $scope.password = null;
		    $scope.passwordConfirm = null;
		}
		else
	    {
			toaster.pop("error","Error","Something went wrong... Please, stay with us");
	    }
	});
	responsePromise.error(function(data, status, headers, config) {
	    toaster.pop("error","Error","Something went wrong with our database... Please, stay with us");
	});	
};
	
$scope.deleteAccount = function () {
	const urlDeleteAccount = "http://" + host + "/ChatRestNoSQLMaven/rest/AccountService/deleteAccount/" + securityKey;
	var responsePromise = $http.get(urlDeleteAccount);	
	responsePromise.success (function(data, status, headers, config) {
		if (data = 1){
			eraseCookieFromAllPaths("uid");
			eraseCookieFromAllPaths("host");
		  	alert = "Your account deleted";
			window.open('../login/login.view.html','_self');
		}
		else
	   	{
			toaster.pop("error","Error","Something went wrong... Please, stay with us");
		}
	});
	responsePromise.error(function(data, status, headers, config) {
		toaster.pop("error","Error","Something went wrong with our database... Please, stay with us");
	});	
};

}]);
}());