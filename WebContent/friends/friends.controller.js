(function () {
    'use strict';

angular.module('myAppName', ['toaster','ngAnimate']).
controller('ContactController', ['$scope', '$http', '$interval', 'toaster',
function($scope, $http, $interval, toaster) {

const securityKey = cookieGet("securityKey");
const host = cookieGet("host");


$scope.goToLink = function(toaster) {
	var match = toaster.body.match(/http[s]?:\/\/[^\s]+/);
	if (match) $window.open(match[0]);
	return true;
};

$scope.requestsFilter = function (item) { 
    if (item.id_dictionary == '10' || item.id_dictionary == '12'){
    	return item;
    }
};

$scope.friendsFilter = function (item) { 
    if (item.id_dictionary == '20' || 
    		item.id_dictionary == '21' || 
    		item.id_dictionary == '22' ||
    		item.id_dictionary == '23'){
    	return true;
    }
    else {return false;}
};

$scope.ignoreFilter = function (item) { 
    if (item.id_dictionary == '30'){
		return item;
	}
};

$scope.getAllUserLinks = function getAllUserLinks(){	
	const getAllUserLinks = "http://" + host + "/ChatRestNoSQLMaven/rest/FriendService/getAllUserLinks/"+securityKey;
	var responsePromiseGetUserLinks = $http.get(getAllUserLinks);	
	responsePromiseGetUserLinks.success(function(data, status, headers, config) {
		$scope.friends = data.records;
	});
};

$scope.processLink = function (url, successMessage) {
	const friendEmail = $scope.searchForFriend;
	var responsePromise = $http.get(url);	
	responsePromise.success(function(data, status, headers, config) {
		if (data == ""){
			$scope.friendEmail = "";
			toaster.pop("success", "Success", successMessage);
		}
		else
		{
			toaster.pop("error", "Error", data);
		}
	});
	responsePromise.error(function(data, status, headers, config) {
	 	toaster.pop("error","Error","Something went wrong with our database... Please, try again later");
	});	
}

$scope.addFriend = function () {	
	const friendEmail = $scope.searchForFriend;
	const urlAddFriend = "http://" + host + "/ChatRestNoSQLMaven/rest/FriendService/addFriend/" + securityKey + "/" + friendEmail;
	$scope.processLink(urlAddFriend, "Request sent");
};

$scope.addSubscription = function () {	
	const friendEmail = $scope.searchForFriend;
	const urlAddFriend = "http://" + host + "/ChatRestNoSQLMaven/rest/FriendService/addSubscription/" + securityKey + "/" + friendEmail;
	$scope.processLink(urlAddFriend, "Subscripted");	
};

$scope.ignoreFriend = function(id) {
	const urlIgnoreFriend = "http://" + host + "/ChatRestNoSQLMaven/rest/FriendService/ignoreFriend/" + securityKey +"/" + id;
	if (confirm("Do you want to ignore friend?") == true) {
		$scope.processLink(urlIgnoreFriend, "Ignored since this moment");
		$scope.getAllUserLinks();				
		}
}; 

$scope.unsubscribe = function(id) {
	const urlUnsubscribe = "http://" + host + "/ChatRestNoSQLMaven/rest/FriendService/unsubscribe/" + securityKey +"/" + id;
	if (confirm("Do you want to unsubscribe?") == true) {
		$scope.processLink(urlUnsubscribe, "Unsubscribed since this moment");
		$scope.getAllUserLinks();				
		}
};

$scope.restoreFriend = function(idFriend) {
	const urlRestoreFriend = "http://" + host + "/ChatRestNoSQLMaven/rest/FriendService/restoreFriend/" + securityKey + "/" + idFriend;
	$scope.processLink(urlRestoreFriend, "Congratulations! You are friends again");
}; 

$scope.restoreSubscription = function(idFriend) {
	const urlRestoreSubscription = "http://" + host + "/ChatRestNoSQLMaven/rest/FriendService/restoreSubscription/" + securityKey + "/" + idFriend;
	$scope.processLink(urlRestoreSubscription, "Congratulations! You are subscripted again");
}; 

$scope.approveFriend = function(idFriend) {
	const urlApproveFriend = "http://" + host + "/ChatRestNoSQLMaven/rest/FriendService/approveFriendshipRequest/" + securityKey + "/" + idFriend;
	$scope.processLink(urlApproveFriend, "Congratulations! You got a new friend");
}; 

$scope.approveSubscription = function(idFriend) {
	const urlSubscribeFriend = "http://" + host + "/ChatRestNoSQLMaven/rest/FriendService/approveSubscriptionOnly/" + securityKey + "/" + idFriend;
	$scope.processLink(urlSubscribeFriend, "Congratulations! You are subscripted");
}; 

$scope.ignoreFrendKeepSubscription = function(idFriend) {
	const urlSubscribeFriend = "http://" + host + "/ChatRestNoSQLMaven/rest/FriendService/ignoreFrendKeepSubscription/" + securityKey + "/" + idFriend;
	$scope.processLink(urlSubscribeFriend, "There is no friendship anymore (you could retore it anytime), but you got a subscription");
}; 


$scope.getAllUserLinks();

var counterOfDBRequests=0;
var timerFriends=$interval(function(){
	if(counterOfDBRequests > 10)
      {
    	$scope.autoRefeshStatus = "Our server is tired. Please, start auto refreshing cycle manually ";
      }
    else{
    	$scope.getAllUserLinks();				
		counterOfDBRequests++;
    }
  },4000);

   $scope.autoRefresh = function () {
   $scope.autoRefeshStatus = "";
   counterOfDBRequests = 0;
};
   
}]);
}()); 