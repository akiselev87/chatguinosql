function adjust_iframe(oFrame)
{
    if(!oFrame) return;

    var win_height;
    var frm_height;     

    // Get page height Firefox/IE
    if(window.innerHeight) win_height = window.innerHeight;
    else if(document.body.clientHeight) win_height = document.body.clientHeight;   

    frm_height = win_height - oFrame.offsetTop - 16; // replace 15 accordingly

    // Set iframe height
    oFrame.style.height = frm_height + "px";
}

(function () {
    'use strict';
	
	angular.module('scopeRegistration', ['toaster','ngAnimate']).
	controller('RegController', ['$scope', '$http', 'toaster',
	
	
	function($scope, $http, toaster) {
	/*const securityKey = cookieGet("securityKey");
	const host = cookieGet("host");
	const method = "GET";
	const url = "http://" + host + "/ChatRestNoSQLMaven/rest/AccountService/getAccountFullName/"+securityKey;
	$scope.iframeurl="../messages/messages.view.html";
	
	var responsePromise = $http.get(url);	
	responsePromise.success
		(function(data, status, headers, config) {
			const textResponse = data;
			$scope.currentUserName = textResponse;
		});
	responsePromise.error(function(data, status, headers, config) {
		toaster.pop("error","Error","Something went wrong with our database... Please, stay with us");
	});	
	*/
		$scope.iframeurl='../news/news.view.html';
		$scope.active='news'; 
		$scope.class="news";
		
		$scope.logout = function () {
			eraseCookieFromAllPaths("uid");
			eraseCookieFromAllPaths("host");
		  	window.open('../login/login.view.html','_self');
		}
		
	}
]);
}());