(function () {
    'use strict';

angular.module('login', ['toaster','ngAnimate']).
controller('LoginController', ['$scope', '$http', 'toaster',
	function($scope, $http, toaster) {
    
$scope.goToLink = function(toaster) {
	var match = toaster.body.match(/http[s]?:\/\/[^\s]+/);
	if (match) $window.open(match[0]);
	return true;
};
	
$scope.login = function () {
const email = escape($scope.email,"UTF-8");
const pwd = escape($scope.password,"UTF-8");
const host = "localhost:8180";
if (email != "undefined" && pwd != "undefined" ){
	const urlLogin = "http://" + host + "/ChatRestNoSQLMaven/rest/AccountService/login/"+email+"/"+pwd;
	var responsePromise = $http.get(urlLogin);	
	
	responsePromise.success(function(data, status, headers, config) {
		const securityKey = data;
		if (securityKey != ""){
			cookiePost("securityKey", securityKey , 1);
			cookiePost("host", host , 1);
			window.open('../index/index.view.html','_self');
		}
		else
		{
		 	toaster.pop("error","Error","Login/Password pair error. Please try again...");
		}
	});
	responsePromise.error(function(data, status, headers, config) {
	 	toaster.pop("error","Error","Something went wrong with our database... Please, try again later");
	});	
}
};
}]);
}());