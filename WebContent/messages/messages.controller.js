(function () {
    'use strict';
    // sorting: https://scotch.io/tutorials/sort-and-filter-a-table-using-angular

angular.module('myAppName', ['ngResource','toaster','ngSanitize','ngFileUpload','ngAnimate']).
controller('ContactController', ['$scope', 'toaster','Upload', '$timeout', '$sce', '$http', '$interval',
function($scope, toaster, Upload, $timeout, $sce, $http, $interval) {

var securityKey = cookieGet("securityKey");	
var host = cookieGet("host");
var showSearchPanel = false;
var showFileUpload = false;
$scope.dialogWithFriend = null;
$scope.hiddenFriendId = null;
var key = 0;

function arrays_equal(a,b) { return !(a<b || b<a); }

$scope.down = function(e) {
  if (key == 0 && e.keyCode == 17){key = 17;}
  else if (key == 17 && e.keyCode != 17 && e.keyCode == 13){
	  $scope.sendMessage();
	  key = 0;
  }
  else if (e.keyCode != 17 && e.keyCode != 13){
	  key = 0;
  }
};

$scope.goToLink = function(toaster) {
	var match = toaster.body.match(/http[s]?:\/\/[^\s]+/);
	if (match) $window.open(match[0]);
	return true;
};

$scope.$watch('files', function () {
   $scope.upload($scope.files);
});

$scope.$watch('file', function () {
    if ($scope.file != null) {
        $scope.files = [$scope.file]; 
    }
});
$scope.log = '';

$scope.getAllFriends = function getAllFriends(){
	var urlAllFriends = "http://" + host + "/ChatRestNoSQLMaven/rest/FriendService/allFriends/"+securityKey;
	var responsePromise = $http.get(urlAllFriends);	
	responsePromise.success
	(function(data, status, headers, config) {
		$scope.contacts = data.records;
	});
	responsePromise.error
	(function(data, status, headers, config) {
		toaster.pop("error","Error","Something went wrong with our database... Please, try again later");
	});	
};

$scope.upload = function (files) {
   var urlUploadFile = "http://" + host + "/ChatRestNoSQLMaven/rest/FileService/uploadMessage";
   var messageText = $scope.messageText;
   var progressPercentage = 0;
   if (files && files.length) {
	   for (var i=0; i < files.length; i++) {
          var file = files[i];
          if (!file.$error) {
             Upload.upload({
               url: urlUploadFile,
               data: {
            	   messageText: messageText,
            	   securityKey: securityKey,
            	   friendId: $scope.hiddenFriendId,
            	   file: file  
               }
           }).then(function (resp) {
             	$timeout(function () {
             		$scope.viewMesseges($scope.hiddenFriendId,"same","same");
             		toaster.pop("success","File uploaded","File " + file.name + " successfully uploaded");
             		$scope.messageText ="";
             	});
           }, function (response) {
                if (response.status > 0) {
                   $scope.errorMsg = response.status + ': ' + response.data;
                   toaster.pop("error","Error","Unable to upload file. Please try again...");
                }
           }, function (evt) {
             	var loaded = evt.loaded;
               	var total = evt.total;
               	var number = 100.0 * (evt.loaded / evt.total);
               	file.progressPercentage = number.toFixed();
           });
          }
	   }
   }
}

$scope.reset = function () {
    $scope.messageText = null;
};

$scope.search = function(){
	showSearchPanel = !showSearchPanel;
	$scope.searchForMessage = null;
	$scope.showSearchPanel = showSearchPanel;
}

$scope.uploadFile = function(){
	showFileUpload = !showFileUpload;
	$scope.showFileUpload = showFileUpload;
}

var getUrlToMessages = "http://"+location.hostname+(location.port ? ':'+location.port: '')+"/ChatGUINoSQL/messages/";
var fullNameForDialog = "";
$scope.dialogWithFriend = "Please select one of your friends (or this is a great place for your advertisement. Just kidding)";
$scope.getAllFriends();

$scope.emoji = function(message){
	return $sce.trustAsHtml(message);
 }

$scope.downloadFile = function(id_message) {
	var friendid = $scope.hiddenFriendId;
	var urlDownloadFile = "http://" + host + "/ChatRestNoSQLMaven/rest/FileService/downloadMessage/" + securityKey + "/" + friendid + "/" + id_message;
	window.location.assign(urlDownloadFile);
};

////////////////////////	

/* Delete Message - not used in UI
$scope.deleteMessage = function(messageid) {
	var friendid = $scope.hiddenFriendId;
	var urlDeleteMessage = "http://" + host + "/ChatRestNoSQLMaven/rest/MessageService/deleteMessage/" + securityKey + "/" + friendid + "/" + messageid;
	var responsePromise = $http.get(urlDeleteMessage);	
	responsePromise.success
	(function(data, status, headers, config) {
		$scope.messages = data.records;
	});
	responsePromise.error(function(data, status, headers, config) {
	    toaster.pop("error","Error","Something went wrong with our database... Please, try again later");
	});	
};
*/

/* 
In case of multiple status change
$scope.processLink = function (url, successMessage) {
	const friendEmail = $scope.searchForFriend;
	var responsePromise = $http.get(url);	
	responsePromise.success(function(data, status, headers, config) {
		if (data == ""){
			$scope.friendEmail = "";
			toaster.pop("success", "Success", successMessage);
		}
		else
		{
			toaster.pop("error", "Error", data);
		}
	});
	responsePromise.error(function(data, status, headers, config) {
	 	toaster.pop("error","Error","Something went wrong with our database... Please, try again later");
	});	
}
*/

$scope.ignoreFriend = function(id) {
	var urlIgnoreFriend = "http://" + host + "/ChatRestNoSQLMaven/rest/FriendService/ignoreFriend/" + securityKey +"/" + id;
	if (confirm("Do you want to ignore friend?") == true) {
		var responsePromise = $http.get(urlIgnoreFriend);	
		responsePromise.success
		(function(data, status, headers, config) {
			if (data = 1){
				toaster.pop("success", "Success", "Ignored since this moment");
				$scope.getAllFriends();
			}
			else
	    	{
				toaster.pop("error","Error","You can't ignore this guy... Please, try again later");
	    	}
	    });
	    responsePromise.error(function(data, status, headers, config) {
	    	toaster.pop("error","Error","Something went wrong with our database... Please, try again later");
	    });
    }
}; 

$scope.viewMesseges = function(id_user_sent, firstName, lastName) {
	var urlViewMessages = "http://" + host + "/ChatRestNoSQLMaven/rest/MessageService/getDialog/"+securityKey+"/"+id_user_sent;
	var responsePromise = $http.get(urlViewMessages);	
	responsePromise.success
	(function(data, status, headers, config) {
		$scope.messages = data.records;
		if (firstName != "same") {
			if (firstName != null){
				fullNameForDialog = "Dialog with " + firstName + " " + lastName;
			}
			$scope.dialogWithFriend = fullNameForDialog;
			$scope.hiddenFriendId = id_user_sent;
		};
	    
	});
	responsePromise.error(function(data, status, headers, config) {
		toaster.pop("error","Error","Something went wrong with our database... Please, try again later");
	});	
};

$scope.sendMessage = function() {
	var friendid = $scope.hiddenFriendId;
	var messageText = $scope.messageText;
	if (messageText != ""){
		var urlSendMessage = "http://" + host + "/ChatRestNoSQLMaven/rest/MessageService/sendMessage/" + securityKey + "/" + friendid + "/" + messageText;
		var responsePromise = $http.get(urlSendMessage);	
		responsePromise.success
			(function(data, status, headers, config) {
				$scope.messageText = null;
				$scope.viewMesseges(friendid);
			});
		responsePromise.error(function(data, status, headers, config) {
		toaster.pop("error","Error","Something went wrong with our database... Please, try again later");
		});	
	}
};


//System autorefreshes refreshes lists//
var counterOfRequestsFriends = 0;
var timerFriends=$interval(function(){
	if(counterOfRequestsFriends > 50)
      {
    	$scope.autoRefeshStatus = "Please refresh dialog manually: ";
      }
    else{
    	$scope.getAllFriends();	
    	counterOfRequestsFriends++;
    }
},50000);
	
var counterOfRequestsMessages=0;
var timer=$interval(function(){
	var friendid = $scope.hiddenFriendId;
	if (friendid){
		if(counterOfRequestsMessages > 100)
	      {
	    	$scope.autoRefeshStatus = "Please refresh dialog manually: ";
	      }
		else{
			$scope.viewMesseges(friendid,"same","same");
			counterOfRequestsMessages++;
		}
	}
 },6000);


$scope.autoRefresh = function () {
   $scope.autoRefeshStatus = "";
   counterOfRequestsFriends = 0;
   counterOfRequestsMessages = 0;
};

}]);
}());