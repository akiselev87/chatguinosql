(function () {
    'use strict';
    // sorting: https://scotch.io/tutorials/sort-and-filter-a-table-using-angular

angular.module('myAppName', ['ngResource','toaster','ngSanitize','ngFileUpload','ngAnimate']).
controller('NewsController', ['$scope', 'toaster','Upload', '$timeout', '$sce', '$http', '$interval',
function($scope, toaster, Upload, $timeout, $sce, $http, $interval) {

var securityKey = cookieGet("securityKey");	
var host = cookieGet("host");
var showSearchPanel = false;
var showFileUpload = false;
$scope.dialogWithFriend = null;
$scope.hiddenFriendId = null;
var key = 0;

var urlViewNews = "http://" + host + "/ChatRestNoSQLMaven/rest/NewsFeedService/getNewsFeed/"+securityKey;
var responsePromise = $http.get(urlViewNews);	
responsePromise.success
(function(data, status, headers, config) {
	$scope.newsFeed = data.records;
});
responsePromise.error(function(data, status, headers, config) {
	toaster.pop("error","Error","Something went wrong with our database... Please, try again later");
});	

function arrays_equal(a,b) { return !(a<b || b<a); }

$scope.down = function(e) {
  if (key == 0 && e.keyCode == 17){key = 17;}
  else if (key == 17 && e.keyCode != 17 && e.keyCode == 13){
	  $scope.sendMessage();
	  key = 0;
  }
  else if (e.keyCode != 17 && e.keyCode != 13){
	  key = 0;
  }
};

$scope.goToLink = function(toaster) {
	var match = toaster.body.match(/http[s]?:\/\/[^\s]+/);
	if (match) $window.open(match[0]);
	return true;
};

$scope.$watch('files', function () {
   $scope.upload($scope.files);
});

$scope.$watch('file', function () {
    if ($scope.file != null) {
        $scope.files = [$scope.file]; 
    }
});
$scope.log = '';

$scope.upload = function (files) {
   var urlUploadFile = "http://" + host + "/ChatRestNoSQLMaven/rest/FileService/uploadNews";
   var messageText = $scope.messageText;
   var progressPercentage = 0;
   if (files && files.length) {
	   for (var i=0; i < files.length; i++) {
          var file = files[i];
          if (!file.$error) {
             Upload.upload({
               url: urlUploadFile,
               data: {
            	   securityKey: securityKey,
            	   friendId: $scope.hiddenFriendId,
            	   file: file,
            	   messageText: messageText
               }
           }).then(function (resp) {
             	$timeout(function () {
             		$scope.viewMesseges($scope.hiddenFriendId,"same","same");
             		toaster.pop("success","File uploaded","File " + file.name + " successfully uploaded");
             		$scope.messageText = "";
             	});
             	viewMesseges(); 
           }, function (response) {
                if (response.status > 0) {
                   $scope.errorMsg = response.status + ': ' + response.data;
                   toaster.pop("error","Error","Unable to upload file. Please try again...");
                }
           }, function (evt) {
             	var loaded = evt.loaded;
               	var total = evt.total;
               	var number = 100.0 * (evt.loaded / evt.total);
               	file.progressPercentage = number.toFixed();
           });
          }
	   }
   }
}

$scope.emoji = function(message){
	return $sce.trustAsHtml(message);
 }

$scope.reset = function () {
    $scope.messageText = null;
};

$scope.uploadFile = function(){
	showFileUpload = !showFileUpload;
	$scope.showFileUpload = showFileUpload;
}

var getUrlToNews = "http://"+location.hostname+(location.port ? ':'+location.port: '')+"/ChatGUINoSQL/News/";

$scope.downloadFile = function(id_message) {
	var urlDownloadFile = "http://" + host + "/ChatRestNoSQLMaven/rest/FileService/downloadNews/" + securityKey + "/" + id_message;
	window.location.assign(urlDownloadFile);
};

$scope.viewMesseges = function() {
	var urlViewNews = "http://" + host + "/ChatRestNoSQLMaven/rest/NewsFeedService/getNewsFeed/"+securityKey;
	var responsePromise = $http.get(urlViewNews);	
	responsePromise.success
	(function(data, status, headers, config) {
		$scope.newsFeed = data.records;
	});
	responsePromise.error(function(data, status, headers, config) {
		toaster.pop("error","Error","Something went wrong with our database... Please, try again later");
	});	
};

$scope.sendMessage = function() {
	var messageText = $scope.messageText;
	const securityLevel = 1;
	if (messageText != "undefined"){
		var urlSendMessage = "http://" + host + "/ChatRestNoSQLMaven/rest/NewsFeedService/sendNews/" + securityKey + "/" + securityLevel + "/" + messageText;
		var responsePromise = $http.get(urlSendMessage);	
		responsePromise.success
			(function(data, status, headers, config) {
				$scope.messageText = null;
				// reload file $scope.viewMesseges(friendid);
			});
		responsePromise.error(function(data, status, headers, config) {
		toaster.pop("error","Error","Something went wrong with our database... Please, try again later");
		});	
	}
};

/* Delete Message - not used in UI
$scope.deleteMessage = function(messageid) {
	var friendid = $scope.hiddenFriendId;
	var urlDeleteMessage = "http://" + host + "/ChatRestNoSQLMaven/rest/Newservice/deleteMessage/" + securityKey + "/" + friendid + "/" + messageid;
	var responsePromise = $http.get(urlDeleteMessage);	
	responsePromise.success
	(function(data, status, headers, config) {
		$scope.News = data.records;
	});
	responsePromise.error(function(data, status, headers, config) {
	    toaster.pop("error","Error","Something went wrong with our database... Please, try again later");
	});	
};
*/

//autorefresh
var counterOfRequestsNews=0;
var timer=$interval(function(){
		if(counterOfRequestsNews > 2)
	      {
	    	$scope.autoRefeshStatus = "Please refresh news manually: ";
	      }
		else{
			$scope.viewMesseges();
			counterOfRequestsNews++;
	}
 },6000);


$scope.autoRefresh = function () {
   $scope.autoRefeshStatus = "";
   counterOfRequestsNews = 0;
};

}]);
}());