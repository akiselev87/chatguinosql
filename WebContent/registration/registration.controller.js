(function () {
    'use strict';

angular.module('scopeRegistration', ['toaster','ngAnimate']).
controller('RegController', ['$scope', '$http','toaster',
function($scope, $http, toaster) {
    	
$scope.goToLink = function(toaster) {
	var match = toaster.body.match(/http[s]?:\/\/[^\s]+/);
	if (match) $window.open(match[0]);
	return true;
};
	
$scope.reset = function () {
    $scope.fname = null;
    $scope.lname = null;
    $scope.email = null;
    $scope.password = null;
    $scope.passwordConfirm = null;
    $scope.fname = null;
    $scope.fname = null;
    $scope.regForm.$setUntouched();
    regForm.fname.$touched = false;
};


$scope.register = function () {
	const email = escape($scope.email,"UTF-8");
	const pwd = escape($scope.password,"UTF-8");
	const pwdconfirm = escape($scope.passwordConfirm,"UTF-8");
	const fname = $scope.fname;
	const lname = $scope.lname;
	if (email != "undefined" && pwd != "undefined" && pwdconfirm != "undefined" && fname != "undefined" && lname != "undefined"){
		const urlRegistration = "http://localhost:8180/ChatRestNoSQLMaven/rest/AccountService/register/"+fname+"/"+lname+"/"+email+"/"+pwd+"/"+pwdconfirm;
		
		var responsePromise = $http.get(urlRegistration);	
			responsePromise.success
			(function(data, status, headers, config) {
				const errorText = data;
				if (errorText == ""){
					toaster.pop("success", "Success", "Registration Completed");
					window.open('../login/login.view.html','_self');
				}
				else
				{
					toaster.pop("error","Error",errorText);
				}
			});
		    responsePromise.error(function(data, status, headers, config) {
		    	toaster.pop("error","Error","Something went wrong with our database... Please, try again later");
		    });	
		}
	};
}]);
}());